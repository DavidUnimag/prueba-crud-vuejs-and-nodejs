'use strict';
module.exports = (sequelize, DataTypes) => {
  const Editoriales = sequelize.define('Editoriales', {
    fecha: DataTypes.STRING,
    titulo: DataTypes.STRING,
    parrafo: DataTypes.STRING,
    usuariosId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'usuarios',
        key: 'id'
    }
  },
  }, {});
  Editoriales.associate = function(models) {
    Editoriales.belongsTo(models.Usuario, {foreignKey: 'usuariosId'});
  };
  return Editoriales;
};