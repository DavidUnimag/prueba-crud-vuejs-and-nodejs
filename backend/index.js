//import mysql from 'mysql';
const mysql = require('mysql2');
const express = require('express');
const bodyParser = require('body-parser');
const cors  = require('cors');
const app = express();
const api = require('./routes');
const database = require('./config/database')
const db = require('./models');
var con = mysql.createConnection(database);


con.connect(function(err) {
    if(err){
        console.log(err);
        return;
    }
    console.log("Conectado a la bd");
})

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use('/api', api);

db.sequelize.sync().then(function() {
    app.listen(3000, function() {
        console.log('Servidor corriendo en puerto 3000');
    })
}).catch( (err)=>{
    console.error(err);
})








