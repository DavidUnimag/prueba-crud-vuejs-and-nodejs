
const express = require('express');
const app = express();
const api = express.Router()
const { singIn } = require('../controllers/authenticationController');
const { getUser, getUsers ,createUsers, updateUsers, deleteUsers } = require('../controllers/usuariosController');
const { getEditorial, getEditoriales ,createEditoriales, updateEditoriales, deleteEditoriales } = require('../controllers/editorialesController');


/** singIn */

api.post('/singIn', singIn);

/** CRUD USUARIOS */
api.get('/getUser', getUser);
api.get('/getUsers', getUsers);
api.post('/createUsers', createUsers);
api.put('/updateUsers', updateUsers);
api.delete('/deleteUsers', deleteUsers);

/** CRUD EDITORIALES */
api.get('/getEditorial', getEditorial);
api.get('/getEditoriales', getEditoriales);
api.post('/createEditoriales', createEditoriales);
api.put('/updateEditoriales', updateEditoriales);
api.delete('/deleteEditoriales', deleteEditoriales);

module.exports = api;
