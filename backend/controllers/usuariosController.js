
const { Usuario } = require('../models')
//ORM
async function getUser(req, res){
    let id = req.query.id;
    try {
        const usuario = await Usuario.findByPk(id);
        return res.status(200).send(usuario);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}

async function getUsers(req, res){
    try {
        const usuarios = await Usuario.findAll();
        return res.status(200).send(usuarios);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }

}

async function createUsers(req, res){
    try {
        await Usuario.create(req.body);
        return res.status(200).send('Se creó el usuario correctamente');
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}

function updateUsers(req, res){
    let nombre = req.body.nombre;
}


function deleteUsers(req, res){
    let id = req.body.id;
}

module.exports = {
    getUser,
    getUsers,
    createUsers,
    updateUsers,
    deleteUsers
}

