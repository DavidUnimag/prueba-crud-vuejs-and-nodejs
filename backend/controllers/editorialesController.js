
const { Usuario, Editoriales } = require('../models');
//ORM
async function getEditorial(req, res){
    let id = req.query.id;
    try {
        const editorial = await Editoriales.findByPk(id);
        return res.status(200).send(editorial);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}

async function getEditoriales(req, res){
    try {
        let editoriales = await Editoriales.findAll({include: [Usuario]});
        return res.status(200).send(editoriales);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}

async function createEditoriales(req, res){
    try {
        await Editoriales.create(req.body);
        return res.status(200).send('Se creó el Editorial correctamente');
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}

function updateEditoriales(req, res){
    let nombre = req.body.nombre;
}


function deleteEditoriales(req, res){
    let id = req.body.id;
}

module.exports = {
    getEditorial,
    getEditoriales,
    createEditoriales,
    updateEditoriales,
    deleteEditoriales
}

