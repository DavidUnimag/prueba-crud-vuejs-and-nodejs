
const { Usuario } = require('../models')
async function singIn(req, res){
    const { nombre, password } = req.body;
    try {
        const usuario = await Usuario.findOne({where: {nombre, password}});
        if(!usuario) return res.status(404).send('No se encontró el usuario');
        return res.status(200).send(usuario);
    } catch (error) {
        console.log(error);
        return res.status(500).send(`Ocurrio un error en el servidor: ${error}`);
    }
}


async function singUp(req, res){
    
}

module.exports = {
    singIn
}

