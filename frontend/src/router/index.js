import Vue from 'vue'
import VueRouter from 'vue-router'
import Usuarios from '../views/usuarios/Usuarios.vue'
import CrearUsuarios from '../views/usuarios/CrearUsuarios.vue'
import Editoriales from '../views/editoriales/Editoriales.vue'
import CrearEditoriales from '../views/editoriales/CrearEditoriales.vue'
import Login from '../views/Login.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Usuarios',
    component: Usuarios
  },
  {
    path: '/crearUsuarios',
    name: 'Crear usuarios',
    component: CrearUsuarios
  },
  {
    path: '/editoriales',
    name: 'Editoriales',
    component: Editoriales
  },
  {
    path: '/crearEditoriales',
    name: 'Crear editorial',
    component: CrearEditoriales
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
